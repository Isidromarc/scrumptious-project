from django.urls import path
from recipes.views import show_recipe, create_recipe, list_recipe, edit_recipe

urlpatterns = [
    path("recipes/<int:id>/", show_recipe, name="show_recipe"),
    path("create/", create_recipe, name="create_recipe"),
    path("", list_recipe, name="main_page"),
    path("recipes/<int:id>/edit/", edit_recipe, name="edit_recipe"),














    path("recipes/<int:id>/edit", edit_recipe, name="edit_recipe")
]
