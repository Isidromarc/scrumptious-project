from django.forms import ModelForm, TextInput, URLInput, Textarea
from recipes.models import Recipe
from recipes.widgetStyling.FormStyling import* #file for style variables

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = (
            "title",
            "picture",
            "description",
        )
        widgets = {
            "title" : TextInput(attrs={
                'placeholder' : 'Title...',
                'style' : title_input + border1,
                }),
            "picture" : URLInput(attrs={
                'placeholder' : 'URL link...',
                'style' : picture_input + border1,
                }),
            "description" : Textarea(attrs={
                'placeholder' : 'Description...',
                'style' : description_input + shadow1 + border1,
                })
        }
