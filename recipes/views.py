from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def list_recipe(request):
    recipe = Recipe.objects.all()
    context = {
        "allrecipes":recipe
    }
    return render(request, "recipes/list.html",context)

def create_recipe(request):
    if request.method == "POST":
        thisform = RecipeForm(request.POST)
        if thisform.is_valid():
            thisform.save()
            return redirect("main_page")
    else:
        thisform = RecipeForm()
        context = {
            "form": thisform
        }
        return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        thisform = RecipeForm(request.POST, instance=recipe)
        if thisform.is_valid():
            thisform.save()
            return redirect("main_page")
    else:
        thisform = RecipeForm(instance=recipe)
        context = {
            "form" : thisform,
            "recipe": recipe,
        }
        return render(request,"recipes/edit.html",context)
